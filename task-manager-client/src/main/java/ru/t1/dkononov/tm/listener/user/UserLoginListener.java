package ru.t1.dkononov.tm.listener.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.UserLoginRequest;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @Getter
    @NotNull
    private final String NAME = "login";

    @Getter
    @NotNull
    private final String DESCRIPTION = "user login";


    @Override
    @EventListener(condition = "@userLoginListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.inLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = TerminalUtil.inLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        @Nullable final String token = getAuthEndpoint().login(request).getToken();
        setToken(token);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }
}

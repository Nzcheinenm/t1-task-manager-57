package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.TaskUpdateByIndexRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIndexListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String DESCRIPTION = "Обновить задачу по индексу.";

    @Getter
    @NotNull
    public final String NAME = "task-update-by-index";

    @Override
    @EventListener(condition = "@taskUpdateByIndexListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[ENTER INDEX]");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("[ENTER NAME]");
        @NotNull final String name = TerminalUtil.inLine();
        System.out.println("[ENTER DESCRIPTION]");
        @NotNull final String description = TerminalUtil.inLine();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setDescription(description);
        request.setName(name);
        getTaskEndpointClient().updateTaskByIndex(request);
    }

}

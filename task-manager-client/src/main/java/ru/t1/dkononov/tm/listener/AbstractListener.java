package ru.t1.dkononov.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.model.ICommand;
import ru.t1.dkononov.tm.api.services.ITokenService;
import ru.t1.dkononov.tm.enumerated.Role;

@Getter
@Setter
@Component
public abstract class AbstractListener implements ICommand {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    @Override
    public String toString() {
        @NotNull final String name = getNAME();
        @Nullable final String argument = getARGUMENT();
        @NotNull final String description = getDESCRIPTION();
        @Nullable String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}

package ru.t1.dkononov.tm.listener.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.UserLockRequest;
import ru.t1.dkononov.tm.enumerated.Role;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class UserLockListener extends AbstractUserListener {

    @Getter
    @NotNull
    private final String DESCRIPTION = "user lock";

    @Getter
    @NotNull
    private final String NAME = "user-lock";

    @Override
    @EventListener(condition = "@userLockListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.inLine();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().lockUser(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}

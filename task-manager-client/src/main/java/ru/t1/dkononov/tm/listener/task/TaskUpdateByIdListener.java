package ru.t1.dkononov.tm.listener.task;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.TaskUpdateByIdRequest;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIdListener extends AbstractTaskListener {

    @Getter
    @NotNull
    public final String NAME = "task-update-by-id";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Обновить задачу по Id.";

    @Override
    @EventListener(condition = "@taskUpdateByIdListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[ENTER ID]");
        @NotNull final String id = TerminalUtil.inLine();
        System.out.println("[ENTER NAME]");
        @NotNull final String name = TerminalUtil.inLine();
        System.out.println("[ENTER DESCRIPTION]");
        @NotNull final String description = TerminalUtil.inLine();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken());
        request.setId(id);
        request.setDescription(description);
        request.setName(name);
        getTaskEndpointClient().updateTaskById(request);
    }

}

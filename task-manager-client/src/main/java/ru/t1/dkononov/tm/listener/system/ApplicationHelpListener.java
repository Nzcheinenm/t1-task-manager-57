package ru.t1.dkononov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.api.model.ICommand;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.Collection;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @Getter
    @NotNull
    public final String ARGUMENT = "-h";

    @Getter
    @NotNull
    public final String DESCRIPTION = "Вывести список команд";

    @Getter
    @NotNull
    public final String NAME = "help";

    @Getter
    @Autowired
    private AbstractListener[] listeners;

    @Override
    @EventListener(condition = "@applicationHelpListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractListener> commands = Arrays.asList(listeners);
        for (@NotNull final ICommand command : commands) System.out.println(command);
    }

}

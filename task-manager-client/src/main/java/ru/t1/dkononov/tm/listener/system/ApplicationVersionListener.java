package ru.t1.dkononov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.dto.request.ApplicationVersionRequest;
import ru.t1.dkononov.tm.dto.response.ApplicationVersionResponse;
import ru.t1.dkononov.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {


    @Getter
    @NotNull
    public final String DESCRIPTION = "Версия приложения";

    @Getter
    @NotNull
    public final String NAME = "version";

    @Getter
    @NotNull
    public final String ARGUMENT = "-version";

    @Override
    @EventListener(condition = "@applicationVersionListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println(getPropertyService().getApplicationVersion());
        @NotNull final ApplicationVersionRequest request = new ApplicationVersionRequest();
        @NotNull final ApplicationVersionResponse serverAboutResponse = getSystemEndpoint().getVersion(request);
        System.out.println(serverAboutResponse.getVersion());
    }

}

package ru.t1.dkononov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkononov.tm.event.ConsoleEvent;
import ru.t1.dkononov.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.Collection;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @Getter
    @NotNull
    public final String DESCRIPTION = "Показать список аргументов";

    @Getter
    @NotNull
    public final String NAME = "arguments";

    @Getter
    @NotNull
    public final String ARGUMENT = "-args";

    @Getter
    @Autowired
    private AbstractListener[] listeners;

    @Override
    @EventListener(condition = "@argumentListListener.getNAME() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final Collection<AbstractListener> commands = Arrays.asList(listeners);
        for (@NotNull final AbstractListener command : commands) {
            @Nullable final String argument = command.getARGUMENT();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}

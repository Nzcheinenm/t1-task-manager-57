package ru.t1.dkononov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkononov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.dkononov.tm.comparator.CreatedComparator;
import ru.t1.dkononov.tm.comparator.StatusComparator;
import ru.t1.dkononov.tm.model.AbstractUserOwnedModel;
import ru.t1.dkononov.tm.model.Project;
import ru.t1.dkononov.tm.model.User;

import java.util.Comparator;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedRepository<E extends AbstractUserOwnedModel> extends AbstractRepository<E> implements IUserOwnedRepository<E> {

    @NotNull
    @Override
    public String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final E entity) {
        if (userId.isEmpty()) return;
        @NotNull final User user = entityManager.find(User.class,userId);
        if (user == null) return;
        entity.setUser(user);
        entityManager.persist(entity);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        if (userId.isEmpty()) return;
        @NotNull final User user = entityManager.find(User.class,userId);
        if (user == null) return;
        entity.setUser(user);
        entityManager.remove(entity);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull E entity) {
        if (userId.isEmpty()) return;
        @NotNull final User user = entityManager.find(User.class,userId);
        if (user == null) return;
        entity.setUser(user);
        entityManager.merge(entity);
    }

}
